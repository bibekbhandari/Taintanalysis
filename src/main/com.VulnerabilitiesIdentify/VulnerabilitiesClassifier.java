import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.NonSparseToSparse;

import java.util.Random;

/**
 * Generic classifier that accepts any weka classifier
 * User can manipulate  splitPercentage, seed, numFolds
 */ 
public class VulnerabilitiesClassifier {

    private final String dataPath; // path of data source //
    private Instances data;
    private Instances trainingInstances;
    private Instances testingInstances;
    private final Classifier classifier; // any weka classifier //
    private double F_score = 0; // model result is not yet obtained //
    private boolean modelRun = false; // flag to check whether the model is evaluated or not //
    public double splitPercentage = 0.70; //default value of percentage to split train and test data--user can modify-//
    public int seed = 48; // default seed value for random --user can modify-- //
    public int numFolds = 10; // default cross validation k folds --user can modify-- //

    /**
     * @param dataPath  //relative or absolute path of data//
     * @param classifier // Any weka classifier//
     */
    VulnerabilitiesClassifier(String dataPath, Classifier classifier) {
        super();
        this.dataPath = dataPath;
        this.classifier = classifier;
    }


    /**
     *  Load DataSet, Set Target Class and Convert sparse attribute to non-sparse
     * @throws Exception
     */
    private void configure_data() throws Exception {

        // Load DataSet //
        DataSource source = new DataSource(dataPath);
        data = source.getDataSet();
        // Set Target Class //
        data.setClassIndex(data.numAttributes() - 1);
        // Convert sparse attribute to non sparse //
        NonSparseToSparse nonSparse = new NonSparseToSparse();
        nonSparse.setInputFormat(data);
        data = Filter.useFilter(data, nonSparse);

    }


    /**
     * Split data to training and testing , set target class, build model
     * @throws Exception
     */
    private void classify() throws Exception {

        // Randomize the dataset //
        data.randomize(new Random(seed));

        // Divide dataset into training and test data //
        int trainingDataSize = (int) Math.round(data.numInstances() * splitPercentage);
        int testDataSize = data.numInstances() - trainingDataSize;

        // Create training data //
        trainingInstances = new Instances(data, 0, trainingDataSize);
        // Create test data //
        testingInstances = new Instances(data, trainingDataSize, testDataSize);

        // Set Target class For each train and Test //
        trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);
        testingInstances.setClassIndex(testingInstances.numAttributes() - 1);
        // Build classifier with training instance //
        classifier.buildClassifier(trainingInstances);

    }

    /**
     * F_measure or F_score is evaluate
     * weka F_measure is a weighted f-measure
     * @throws Exception
     */
    private void evaluate_Fscore() throws Exception {
        
        Evaluation evaluation = new Evaluation(trainingInstances);
        // Perform cross validation with training instances as per given number of folds //
        evaluation.crossValidateModel(classifier, trainingInstances, numFolds, new Random(seed));
        // Evaluate model with testInstances
        evaluation.evaluateModel(classifier, testingInstances);

        // Evaluate Precision, Recall and F_score from confusion matrix //
        double[][] confusionMatrix = evaluation.confusionMatrix();
        // Precision = True Positive/ (True Positive+ False Positive) //
        double Precision = confusionMatrix[0][0] / (confusionMatrix[0][0] + confusionMatrix[0][1]);
        // Recall = True Positive/ (True Positive+ False Negative) //
        double Recall = confusionMatrix[0][0] / (confusionMatrix[0][0] + confusionMatrix[1][0]);
        // Precision = True Positive/ (True Positive+ False Positive) //
        F_score = 2 * Precision * Recall / (Precision + Recall);

    }

    /**
     * Run Classifier in stepwise fashion
     * @throws Exception
     */
    public void runclassify() throws Exception {

        // step1: Data preparation //
        configure_data();
        // Build model //
        classify();
        // Evaluate model //
        evaluate_Fscore();
        modelRun = true;
    }


    /**
     * @return F_measure of a specific classifier
     * @throws Exception
     */
    public double get_Fscore() throws Exception {
        if (!modelRun) {
            throw new Exception("classifier has not run to classify");
        }
        return F_score;
    }


}
