import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.Bagging;
import weka.classifiers.meta.Vote;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;

public class ClassifierAnalysis {
    public static void main(String[] args) {
        // Provide a data path v//
        String dataPath = "src/main/com.Vulnerabilities.data/vulnerable-methods-dataset.arff";
        // Number of iteration to run a classifier //
        int no_of_runs = 10;
        //--------------------------------------------------------------------------------------------------------------
        // Naive Bayes Classifier                                                                                    //
        //--------------------------------------------------------------------------------------------------------------
        System.out.print("Naive Bayes Classifier: ");
        Classifier classifier;
        VulnerabilitiesClassifier vulnerabilitiesClassifier;
        for (int run = 0; run <= no_of_runs; run++) {
            classifier = new NaiveBayes();
            // Create vulnerabilities
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, classifier);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf("%.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        // Support Vector Machine
        //--------------------------------------------------------------------------------------------------------------
        System.out.print("Support Vector Machine: ");
        for (int run = 0; run <= no_of_runs; run++) {
            classifier = new SMO();
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, classifier);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf(" %.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        // Random Forest
        //--------------------------------------------------------------------------------------------------------------
        System.out.print("Random Forest: ");
        for (int run = 0; run <= no_of_runs; run++) {
            classifier = new RandomForest();
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, classifier);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf(" %.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        // Meta ClassifierS
        //--------------------------------------------------------------------------------------------------------------
        //Adabost
        //--------------------------------------------------------------------------------------------------------------
        System.out.print("AdaBoostM1: ");
        for (int run = 0; run <= no_of_runs; run++) {
            // Class for boosting a nominal class classifier //
            classifier = new AdaBoostM1(); // internally DecisionStump weak classifier is used
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, classifier);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf(" %.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        // Bagging
        //--------------------------------------------------------------------------------------------------------------
        // Class for bagging a classifier to reduce variance. Can do classification depending on the base learner
        System.out.print("Bagging: ");
        for (int run = 0; run <= no_of_runs; run++) {
            Bagging bagging = new Bagging();
            bagging.setClassifier(new J48());
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, bagging);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf(" %.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        // vote
        //--------------------------------------------------------------------------------------------------------------
        System.out.print("Vote multiple classifier: ");
        for (int run = 0; run <= no_of_runs; run++) {
            // Class for combining classifiers. Different combinations of probability estimates for classification //
            // are available  //
            Vote vote = new Vote();
            Classifier[] classifiers = {new NaiveBayes(), new RandomForest(), new SMO()};
            vote.setClassifiers(classifiers);
            vulnerabilitiesClassifier = new VulnerabilitiesClassifier(dataPath, vote);
            vulnerabilitiesClassifier.seed = run;
            try {
                // Run Classifier in stepwise order fashion //
                vulnerabilitiesClassifier.runclassify();
                // F_score is limited to 3 decimal places //
                System.out.printf(" %.3f ", vulnerabilitiesClassifier.get_Fscore());
            } catch (Exception e) {
                e.fillInStackTrace();
            }
        }
        System.out.println();
        //--------------------------------------------------------------------------------------------------------------
        //                                      End
        //--------------------------------------------------------------------------------------------------------------

    }
}
